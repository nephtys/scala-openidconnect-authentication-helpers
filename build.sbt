name := "openidconnectexample"

version := "1.0"

scalaVersion := "2.11.8"



// https://mvnrepository.com/artifact/com.typesafe.akka/akka-http-experimental_2.11
libraryDependencies += "com.typesafe.akka" %% "akka-http-experimental" % "2.4.11"


// https://mvnrepository.com/artifact/com.google.api-client/google-api-client
//libraryDependencies += "com.google.api-client" % "google-api-client" % "1.22.0"

libraryDependencies += "com.lihaoyi" %% "upickle" % "0.4.3"


libraryDependencies +=  "org.scalaj" %% "scalaj-http" % "2.3.0"
