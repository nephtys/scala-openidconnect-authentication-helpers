package nephty.oidc.helper

import java.util.Base64

import nephty.oidc.helper.GoogleAuth.{AccessTokenResponse, RemoteVerifiedIdentityToken}
import sun.misc.BASE64Decoder

import scala.util.Try
import scalaj.http.Http
import upickle.default._

/**
  * Created by nephtys on 11/4/16.
  */
class GoogleAuth(val clientCredentials : GoogleAuth.ClientCredentialFile, val redirectFile: GoogleAuth.RedirectFile) {
  //TODO: make sure everything is URL encoded (so replacing whitespace too!)

  def googleAuthURL = clientCredentials.web.auth_uri

  val responseType = """code""" //basically fix
  val scope = """openid%20email""" //minimal information required for openid connect

  def redirectURL = redirectFile.redirecturl
  def clientId = clientCredentials.web.client_id
  def clientSecret = clientCredentials.web.client_secret

  def authRequestUrl(
                      state : String = "42"
                      //can be anything, returned to us at the redirecturl as query parameter
                    ) =
    s"""$googleAuthURL?client_id=$clientId&response_type=$responseType&""" +
      s"""scope=$scope&redirect_uri=$redirectURL&state=$state"""



  def postCodeURL : String = clientCredentials.web.token_uri
  val grant_type : String = """authorization_code"""

  def verify(identityToken : String) : Try[RemoteVerifiedIdentityToken] = {
    Try(read[RemoteVerifiedIdentityToken](Http(GoogleAuth.idTokenVerifyUrl + identityToken.trim).asString.body))
  }



  private def postFormMap(code : String) : Map[String, String] = {
    val map : Map[String, String] = Map(
      "code" -> code,
      "client_id" -> clientId,
      "client_secret" -> clientSecret,
      "redirect_uri" -> redirectURL,
      "grant_type" -> grant_type
    )
    map
  }
  private def buildPOSTBody(code : String) : String = {
    val map = postFormMap(code)
    map.toSeq.map(a => a._1 + a._2).mkString("&")
  }

  import upickle.default._

  def postCodeForAccessAndIDToken(code : String) : Try[AccessTokenResponse] =  Try{
    val response = Http(postCodeURL).postForm(postFormMap(code).toSeq).asString.body
    println(s"response = $response")
    read[GoogleAuth.AccessTokenResponse](response)
  }

}

object GoogleAuth {
  val idTokenVerifyUrl : String = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token="
  val payloadIndex : Int = 1
  val tokenSeparator = '.'
  def decodeBase64(text : String) : String = {
    new String(Base64.getUrlDecoder.decode(text), "utf-8")
  }

  def extractEmail(token : String) : Option[String] = {
    Try(read[IdentityToken](token.split(tokenSeparator)(payloadIndex)).email).toOption
  }

  case class AccessTokenResponse(
                                  access_token : String,
                                  id_token : String,
                                  expires_in : Double,
                                  token_type : String) {
    assert(token_type.equals("Bearer"))

    def extractEmail: Option[String] ={
      GoogleAuth.extractEmail(id_token)
    }
  }

  case class RedirectFile(
                             redirecturl: String,
                             raw_host : String,
                             port : Int
                           )

  case class IdentityToken(
                            iss: String,
                            iat: Double,
                            exp: Double,
                            at_hash: String,
                            aud: String,
                            sub: String,
                            email_verified: Boolean,
                            azp: String,
                            email: String
                          )

  case class RemoteVerifiedIdentityToken(
                             iss: String,
                             iat: Long,
                             exp: Long,
                             at_hash: String,
                             aud: String,
                             sub: String,
                             email_verified: String,
                             azp: String,
                             email: String,
                             alg: String,
                             kid: String
                           ) {
    def testIss : Boolean = {
      println(iss)
      val b1 = iss.equals("https://accounts.google.com")
      val b2 = iss.equals("accounts.google.com")
      println(b1)
      println(b2)
      b1 | b2
    }

    def notExpired : Boolean = {
      val time = System.currentTimeMillis / 1000
      println("comparing timestamps:")
      println(time)
      println(exp)
      time < exp
    }
  }

  case class Web(
                  client_id: String,
                  project_id: String,
                  auth_uri: String,
                  token_uri: String,
                  auth_provider_x509_cert_url: String,
                  client_secret: String
                )
  case class ClientCredentialFile(web: Web) //represent format downloadable from Google API
}