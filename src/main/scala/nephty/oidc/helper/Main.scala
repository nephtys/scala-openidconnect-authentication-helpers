package nephty.oidc.helper

import java.nio.file.{Files, Paths}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.stream.ActorMaterializer
import upickle.default._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.io.StdIn
import scala.util.{Failure, Success, Try}


object Main extends App {

  //TODO: implement a Microsoft helper too
  //TODO: implement a Amazon helper too




  private val pathToRedirectURL = "./MyRedirectURL.json"
  private val pathToGoogleCredentials = "./GoogleAPICredentials.json"

  println("Hello World")

  private val redirectURL = loadFileAndParse[GoogleAuth.RedirectFile](pathToRedirectURL)
  private val googleCredentials = loadFileAndParse[GoogleAuth.ClientCredentialFile](pathToGoogleCredentials)

  val google = new GoogleAuth(googleCredentials, redirectURL)

  implicit val actorSystem = ActorSystem("system")
  implicit val actorMaterializer = ActorMaterializer()

  val route : Route = pathSingleSlash {
    get {
      complete {
        "Hello world from openidconnect-example!"
      }
    }
  } ~ path("login") {
    get {
      println(s"Received something at login endpoint, redirecting to ${google.authRequestUrl()}")
      //redirect to google
      redirect(google.authRequestUrl(), StatusCodes.TemporaryRedirect)
    }
  } ~ path("redirectendpoint") {
    get {
      parameters('state, 'code
        , 'authuser, 'session_state, 'prompt
      ) {(state, code
                                                , authuser, session_state, prompt
                                                               ) =>
      //TODO: extract state and code query parameters
      //TODO: exchange code for access and ID token via HTTPS POST (with client_id and client_secret)
      println("Received something at redirectedendpoint")
        println(s"code = $code")
        println(s"state = $state")
        println(google.postCodeForAccessAndIDToken(code))
      complete {
        "Hello from redirectedendpoint"
      }
      }
    }
  }

  val bindingFuture = Http().bindAndHandle(route, redirectURL.raw_host, redirectURL.port)
  println(s"Server online at http://${redirectURL.raw_host}:${redirectURL.port}/\n\nClick on http://${redirectURL
    .raw_host}:${redirectURL.port}/login\n\n to login via Google RETURN to " +
    s"stop." +
    s"..")
  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => actorSystem.terminate()) // and shutdown when done

  def loadFileAndParse[T](filepath : String)(implicit reader: Reader[T]) : T = {
    import scala.collection.JavaConversions._

    val rawContent : Try[String] = Try {
      Files.readAllLines(Paths.get(filepath)).mkString
    }
    rawContent match {
      case Success(a) => {
        val parsed : Try[T] = Try {
          read[T](a)
        }
        parsed match {
          case Success(t) => {
            t
          }
          case Failure(e) => {
            println(e.getMessage)
            throw new Exception(s"Could not parse file contents of file $filepath!")
          }
        }
      }
      case Failure(e) => {
        println(e.getMessage)
        throw new Exception(s"Could not find or read file $filepath!")
      }
    }
  }
}